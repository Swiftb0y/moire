# just manual: https://github.com/casey/just/#readme

_default:
    @just --list

# Set up (and update) tooling
setup:
    # Ignore rustup failures, because not everyone might use it
    rustup self update || true
    # cargo-edit is needed for `cargo upgrade`
    cargo install cargo-edit
    pip install -U pre-commit
    pre-commit autoupdate
    pre-commit install --hook-type commit-msg --hook-type pre-commit

# Format source code
fmt:
    cargo fmt --all

# Run pre-commit hooks
pre-commit:
    pre-commit run --all-files

# Upgrade (and update) dependencies
upgrade:
    # TODO: Remove rust-jack from exclusions after
    # <https://github.com/RustAudio/rust-jack/pull/172>
    # has been resolved and the version is no longer
    # pinned to v0.9.x.
    cargo upgrade --workspace \
        --exclude jack
    cargo update
    cargo upgrade --workspace --to-lockfile \
        --exclude jack

# Check all crates individually (takes a long time)
check:
    cargo check --locked --all-targets -p moire
    cargo check --locked --all-targets -p moire-app
    cargo check --locked --all-targets -p moire-audio-dsp
    cargo check --locked --all-targets -p moire-audio-io
    cargo check --locked --all-targets -p moire-library-backend
    cargo check --locked --all-targets -p moire-library-frontend
    cargo check --locked --all-targets -p moire-ui
    cargo check --locked --all-targets -p moire-ui-generated

# Run clippy on the workspace (both dev and release profile)
clippy:
    cargo clippy --locked --workspace --all-targets --no-deps --profile dev -- -D warnings --cap-lints warn
    cargo clippy --locked --workspace --all-targets --no-deps --profile release -- -D warnings --cap-lints warn

# Fix lint warnings
fix:
    cargo fix --locked --workspace --all-targets --all-features
    cargo clippy --locked --workspace --no-deps --all-targets --all-features --fix

# Run tests
test:
    RUST_BACKTRACE=1 cargo test --locked --workspace -- --nocapture
