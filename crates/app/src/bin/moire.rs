use std::{
    fs,
    path::{Path, PathBuf},
    rc::Rc,
    thread,
    time::Duration,
};

use directories::ProjectDirs;
use rtrb::RingBuffer;

use moire_audio_dsp::{engine as audio_engine, Engine as AudioEngine};
use moire_audio_io::jack::JackBackend;
use moire_library_backend::Backend as LibraryBackend;
use moire_ui::Gui;

// hack around https://github.com/Windfisch/rust-assert-no-alloc/issues/7
#[cfg(not(all(windows, target_env = "gnu")))]
#[global_allocator]
static A: assert_no_alloc::AllocDisabler = assert_no_alloc::AllocDisabler;

const GC_PERIOD: Duration = Duration::from_secs(1);

#[must_use]
pub fn app_name() -> &'static str {
    // The crate name differs from the desired app name, i.e. we cannot
    // use `env!("CARGO_PKG_NAME")` for this purpose.
    "moire"
}

#[must_use]
pub fn app_dirs() -> Option<ProjectDirs> {
    ProjectDirs::from("", "", app_name())
}

#[must_use]
fn init_config_dir(app_dirs: &ProjectDirs) -> &Path {
    let app_config_dir = app_dirs.config_dir();
    if let Err(err) = fs::create_dir_all(&app_config_dir) {
        log::warn!(
            "Failed to create config directory '{dir}': {err}",
            dir = app_config_dir.display(),
        );
    }
    app_config_dir
}

fn app_config_dir() -> Option<PathBuf> {
    app_dirs()
        .as_ref()
        .map(init_config_dir)
        .map(Path::to_path_buf)
}

#[tokio::main]
async fn main() {
    pretty_env_logger::init();

    let config_dir = match app_config_dir() {
        Some(config_dir) => config_dir,
        None => {
            log::error!("Config directory is unavailable");
            return;
        }
    };
    if !config_dir.exists() {
        log::error!(
            "Config directory '{dir_path}' does not exist",
            dir_path = config_dir.display()
        );
        return;
    }
    let config_dir_readonly = match config_dir
        .metadata()
        .map(|metadata| metadata.permissions().readonly())
    {
        Ok(readonly) => readonly,
        Err(err) => {
            log::error!("Failed to query permissions of config directory: {err}");
            // Assume that the directory is writable, i.e. not read-only
            false
        }
    };
    if config_dir_readonly {
        log::warn!(
            "Config directory (read-only): {dir_path}",
            dir_path = config_dir.display()
        );
    } else {
        log::info!(
            "Config directory: {dir_path}",
            dir_path = config_dir.display()
        );
    }

    let aoide_initial_settings =
        match aoide_desktop_app::settings::State::restore_from_parent_dir(&config_dir) {
            Ok(settings) => settings,
            Err(err) => {
                log::error!("Failed to restore aoide settings: {err}");
                return;
            }
        };
    let aoide_db_config = match aoide_initial_settings.create_database_config() {
        Ok(db_config) => db_config,
        Err(err) => {
            log::error!("Failed to create aoide database config: {err}");
            return;
        }
    };
    log::debug!("Commissioning library backend: {aoide_db_config:?}");
    let library_backend = match LibraryBackend::commission(aoide_db_config) {
        Ok(library_backend) => library_backend,
        Err(err) => {
            log::error!("Failed to commission library backend: {err}");
            return;
        }
    };

    let mut gc = basedrop::Collector::new();
    let gc_handle = gc.handle();
    thread::spawn(move || loop {
        gc.collect();
        thread::sleep(GC_PERIOD);
    });

    let (to_audio_tx, from_gui_rx) = RingBuffer::<audio_engine::Command>::new(2048);
    let (to_gui_tx, from_audio_rx) = RingBuffer::<audio_engine::Event>::new(2048);
    let audio_engine = AudioEngine::new(from_gui_rx, to_gui_tx);
    let (jack_backend, jack_process_tx) =
        JackBackend::new(gc_handle.clone(), audio_engine, app_name());
    let jack_backend = Rc::new(jack_backend);

    let gui = Gui::new(
        config_dir,
        library_backend,
        aoide_initial_settings,
        to_audio_tx,
        from_audio_rx,
        jack_process_tx,
        Rc::clone(&jack_backend),
        gc_handle,
    );

    gui.run();
}
