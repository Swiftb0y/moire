use aoide_backend_embedded::track as api;

use aoide_core::{
    collection::EntityUid as CollectionUid,
    media::content::ContentPath,
    track::{Entity, EntityUid},
};

use crate::{Backend, Pagination, Result};

pub async fn load_one(backend: &Backend, entity_uid: EntityUid) -> Result<Entity> {
    api::load_one(backend.environment().db_gatekeeper(), entity_uid).await
}

pub async fn load_many<I>(backend: &Backend, entity_uids: I) -> Result<Vec<Entity>>
where
    I: IntoIterator<Item = EntityUid> + Send + 'static,
{
    api::load_many(backend.environment().db_gatekeeper(), entity_uids).await
}

pub async fn search(
    backend: &Backend,
    collection_uid: CollectionUid,
    params: aoide_core_api::track::search::Params,
    pagination: Pagination,
) -> Result<Vec<Entity>> {
    api::search(
        backend.environment().db_gatekeeper(),
        collection_uid,
        params,
        pagination,
    )
    .await
}

pub async fn replace_many_by_media_source_content_path<I>(
    backend: &Backend,
    collection_uid: CollectionUid,
    params: aoide_usecases::track::replace::Params,
    validated_track_iter: I,
) -> Result<aoide_core_api::track::replace::Summary>
where
    I: IntoIterator<Item = aoide_usecases::track::ValidatedInput> + Send + 'static,
{
    api::replace_many_by_media_source_content_path(
        backend.environment().db_gatekeeper(),
        collection_uid,
        params,
        validated_track_iter,
    )
    .await
}

pub async fn import_and_replace_many_by_local_file_path<I>(
    backend: &Backend,
    collection_uid: CollectionUid,
    params: aoide_usecases::track::import_and_replace::Params,
    content_path_iter: I,
    expected_content_path_count: Option<usize>,
) -> Result<aoide_usecases::track::import_and_replace::Outcome>
where
    I: IntoIterator<Item = ContentPath> + Send + 'static,
{
    api::import_and_replace_many_by_local_file_path(
        backend.environment().db_gatekeeper(),
        collection_uid,
        params,
        content_path_iter,
        expected_content_path_count,
    )
    .await
}
